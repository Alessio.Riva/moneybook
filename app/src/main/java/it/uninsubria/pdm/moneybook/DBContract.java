//Riva Alessio 726395

package it.uninsubria.pdm.moneybook;

import android.provider.BaseColumns;

public class DBContract {



        // database
        public static final String DATABASE_NAME = "MoneyBookDB";
        public static final int DATABASE_VERSION = 2;


        private DBContract() {
        }

        //tab name + colums
        public static abstract class TransactionItems implements BaseColumns {
            public static final String _ID = "ID";
            public static final String TABLE_NAME = "MoneyBook";
            public static final String COLUMN_NAME_TYPE = "Type";
            public static final String COLUMN_NAME_NOTE = "Note";
            public static final String COLUMN_NAME_AMOUNT = "Amount";
            public static final String COLUMN_NAME_CREATION_DATE = "Date";

        }

}
