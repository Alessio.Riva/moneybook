//Riva Alessio 726395
package it.uninsubria.pdm.moneybook;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Review extends AppCompatActivity implements View.OnClickListener{
    ArrayList<Transaction> transactions;
    TextView TextViewValueTotal;
    Button buttonAll,buttonSalary,buttonExtra,buttonTax,buttonFood,buttonTravel,buttonOther;
    private DBAdapter dbAdapter;
    //ListView listView;
    // CustomArrayAdapter listViewAdapter;

    LinearLayoutManager mLinearLayoutManager;
    RecyclerView listView;
    RecyclerView.Adapter Adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review);
        Toast toast = Toast.makeText(this, "Swipe to delete,\nLong press  to modify", Toast.LENGTH_LONG);
        toast.show();

        transactions = new ArrayList<Transaction>();
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        transactions =getAllItems();

        /*listView = (ListView)findViewById(R.id.ListView);
        listViewAdapter= new CustomArrayAdapter(getApplicationContext(),R.layout.listviewitem,transactions);
        listView.setAdapter(listViewAdapter);*/

        listView = (RecyclerView)findViewById(R.id.ListView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(mLinearLayoutManager);
        Adapter = new myAdapter(transactions,this);
        listView.setAdapter(Adapter);
        TextViewValueTotal=(TextView)findViewById(R.id.textViewTotalReview);
        setRecyclerViewItemTouchListener();


        buttonAll=findViewById(R.id.buttonAll);
        buttonSalary=findViewById(R.id.buttonSalary);
        buttonExtra=findViewById(R.id.buttonExtra);
        buttonTax=findViewById(R.id.buttonTax);
        buttonFood=findViewById(R.id.buttonFood);
        buttonTravel=findViewById(R.id.buttonTravel);
        buttonOther=findViewById(R.id.buttonOther);
        buttonAll.setOnClickListener(this);
        buttonSalary.setOnClickListener(this);
        buttonExtra.setOnClickListener(this);
        buttonTax.setOnClickListener(this);
        buttonFood.setOnClickListener(this);
        buttonTravel.setOnClickListener(this);
        buttonOther.setOnClickListener(this);

        buttonAll.setClickable(false);
        buttonAll.setTextColor(getResources().getColor(R.color.textBtnSelected));

        setTotal();


    }

    @Override
    protected void onResume() {
        super.onResume();
        //dbAdapter.open();
        transactions.clear();
        transactions.addAll(dbAdapter.getAllItems());
        Adapter.notifyDataSetChanged();
        setTotal();
        buttonAll.setClickable(false);
        buttonSalary.setClickable(true);
        buttonExtra.setClickable(true);
        buttonTax.setClickable(true);
        buttonFood.setClickable(true);
        buttonTravel.setClickable(true);
        buttonOther.setClickable(true);
        buttonAll.setTextColor(getResources().getColor(R.color.textBtnSelected));
        buttonSalary.setTextColor(getResources().getColor(R.color.textBtn));
        buttonExtra.setTextColor(getResources().getColor(R.color.textBtn));
        buttonTax.setTextColor(getResources().getColor(R.color.textBtn));
        buttonFood.setTextColor(getResources().getColor(R.color.textBtn));
        buttonTravel.setTextColor(getResources().getColor(R.color.textBtn));
        buttonOther.setTextColor(getResources().getColor(R.color.textBtn));
        //listViewAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onPause() {
        super.onPause();
       // dbAdapter.close();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //dbAdapter.close();

    }

    public void setTotal(){
        float in,out,total;
        in=dbAdapter.getIncome();
        out=dbAdapter.getOutcome();
        total=in-out;
        TextViewValueTotal.setText(String.valueOf(total));

    }
    public ArrayList<Transaction> getAllItems(){
        ArrayList<Transaction> AllItems;
        AllItems=dbAdapter.getAllItems();
        Log.v("NUMBER", "size"+String.valueOf(transactions.size()));
        return AllItems;
    }
    private void setRecyclerViewItemTouchListener() {
        //create callback pass 0 to drag direction and value to swipe direction

        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
        {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder viewHolder1) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {

                //called when an item is swiped left or right, remove item at position and notify the recycler adapter
                int position = viewHolder.getAdapterPosition();
                Transaction item = transactions.remove(position);
                // delete the item from the DB
                dbAdapter.deleteTransaction(item.getID());
                listView.getAdapter().notifyItemRemoved(position);
                setTotal();

            }
            //on swipe at 80% of screen
            @Override
            public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder){
                return 0.8f;
            }


        };
        // inizialization of itemtouchhelper and attach to recyclerview
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(listView);

    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.buttonAll){
            buttonAll.setClickable(false);
            buttonSalary.setClickable(true);
            buttonExtra.setClickable(true);
            buttonTax.setClickable(true);
            buttonFood.setClickable(true);
            buttonTravel.setClickable(true);
            buttonOther.setClickable(true);
            buttonAll.setTextColor(getResources().getColor(R.color.textBtnSelected));
            buttonSalary.setTextColor(getResources().getColor(R.color.textBtn));
            buttonExtra.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTax.setTextColor(getResources().getColor(R.color.textBtn));
            buttonFood.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTravel.setTextColor(getResources().getColor(R.color.textBtn));
            buttonOther.setTextColor(getResources().getColor(R.color.textBtn));

            transactions.clear();
            transactions.addAll(dbAdapter.getAllItems());
            Adapter.notifyDataSetChanged();

        }else  if(v.getId()== R.id.buttonSalary){
            buttonAll.setClickable(true);
            buttonSalary.setClickable(false);
            buttonExtra.setClickable(true);
            buttonTax.setClickable(true);
            buttonFood.setClickable(true);
            buttonTravel.setClickable(true);
            buttonOther.setClickable(true);
            buttonAll.setTextColor(getResources().getColor(R.color.textBtn));
            buttonSalary.setTextColor(getResources().getColor(R.color.textBtnSelected));
            buttonExtra.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTax.setTextColor(getResources().getColor(R.color.textBtn));
            buttonFood.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTravel.setTextColor(getResources().getColor(R.color.textBtn));
            buttonOther.setTextColor(getResources().getColor(R.color.textBtn));

            transactions.clear();
            transactions.addAll(dbAdapter.getAllSalary());
            Adapter.notifyDataSetChanged();

        }else  if(v.getId()== R.id.buttonExtra){
            buttonAll.setClickable(true);
            buttonSalary.setClickable(true);
            buttonExtra.setClickable(false);
            buttonTax.setClickable(true);
            buttonFood.setClickable(true);
            buttonTravel.setClickable(true);
            buttonOther.setClickable(true);
            buttonAll.setTextColor(getResources().getColor(R.color.textBtn));
            buttonSalary.setTextColor(getResources().getColor(R.color.textBtn));
            buttonExtra.setTextColor(getResources().getColor(R.color.textBtnSelected));
            buttonTax.setTextColor(getResources().getColor(R.color.textBtn));
            buttonFood.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTravel.setTextColor(getResources().getColor(R.color.textBtn));
            buttonOther.setTextColor(getResources().getColor(R.color.textBtn));

            transactions.clear();
            transactions.addAll(dbAdapter.getAllExtra());
            Adapter.notifyDataSetChanged();

        }else  if(v.getId()== R.id.buttonTax){
            buttonAll.setClickable(true);
            buttonSalary.setClickable(true);
            buttonExtra.setClickable(true);
            buttonTax.setClickable(false);
            buttonFood.setClickable(true);
            buttonTravel.setClickable(true);
            buttonOther.setClickable(true);
            buttonAll.setTextColor(getResources().getColor(R.color.textBtn));
            buttonSalary.setTextColor(getResources().getColor(R.color.textBtn));
            buttonExtra.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTax.setTextColor(getResources().getColor(R.color.textBtnSelected));
            buttonFood.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTravel.setTextColor(getResources().getColor(R.color.textBtn));
            buttonOther.setTextColor(getResources().getColor(R.color.textBtn));

            transactions.clear();
            transactions.addAll(dbAdapter.getAllTax());
            Adapter.notifyDataSetChanged();

        }else  if(v.getId()== R.id.buttonFood){
            buttonAll.setClickable(true);
            buttonSalary.setClickable(true);
            buttonExtra.setClickable(true);
            buttonTax.setClickable(true);
            buttonFood.setClickable(false);
            buttonTravel.setClickable(true);
            buttonOther.setClickable(true);
            buttonAll.setTextColor(getResources().getColor(R.color.textBtn));
            buttonSalary.setTextColor(getResources().getColor(R.color.textBtn));
            buttonExtra.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTax.setTextColor(getResources().getColor(R.color.textBtn));
            buttonFood.setTextColor(getResources().getColor(R.color.textBtnSelected));
            buttonTravel.setTextColor(getResources().getColor(R.color.textBtn));
            buttonOther.setTextColor(getResources().getColor(R.color.textBtn));

            transactions.clear();
            transactions.addAll(dbAdapter.getAllFood());
            Adapter.notifyDataSetChanged();

        }else  if(v.getId()== R.id.buttonTravel){
            buttonAll.setClickable(true);
            buttonSalary.setClickable(true);
            buttonExtra.setClickable(true);
            buttonTax.setClickable(true);
            buttonFood.setClickable(true);
            buttonTravel.setClickable(false);
            buttonOther.setClickable(true);
            buttonAll.setTextColor(getResources().getColor(R.color.textBtn));
            buttonSalary.setTextColor(getResources().getColor(R.color.textBtn));
            buttonExtra.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTax.setTextColor(getResources().getColor(R.color.textBtn));
            buttonFood.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTravel.setTextColor(getResources().getColor(R.color.textBtnSelected));
            buttonOther.setTextColor(getResources().getColor(R.color.textBtn));

            transactions.clear();
            transactions.addAll(dbAdapter.getAllTravel());
            Adapter.notifyDataSetChanged();

        }else  if(v.getId()== R.id.buttonOther){
            buttonAll.setClickable(true);
            buttonSalary.setClickable(true);
            buttonExtra.setClickable(true);
            buttonTax.setClickable(true);
            buttonFood.setClickable(true);
            buttonTravel.setClickable(true);
            buttonOther.setClickable(false);
            buttonAll.setTextColor(getResources().getColor(R.color.textBtn));
            buttonSalary.setTextColor(getResources().getColor(R.color.textBtn));
            buttonExtra.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTax.setTextColor(getResources().getColor(R.color.textBtn));
            buttonFood.setTextColor(getResources().getColor(R.color.textBtn));
            buttonTravel.setTextColor(getResources().getColor(R.color.textBtn));
            buttonOther.setTextColor(getResources().getColor(R.color.textBtnSelected));

            transactions.clear();
            transactions.addAll(dbAdapter.getAllOther());
            Adapter.notifyDataSetChanged();

        }
    }
}
