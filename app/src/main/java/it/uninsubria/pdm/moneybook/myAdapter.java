//Riva Alessio 726395
package it.uninsubria.pdm.moneybook;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public  class myAdapter extends RecyclerView.Adapter<myAdapter.TransactionItemHolder>{

    ArrayList<Transaction> Items;
    Context context;


    public myAdapter(ArrayList<Transaction> Items, Context context){
        this.Items=Items;
        this.context=context;
    }
    @Override
    public myAdapter.TransactionItemHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listviewitem, parent, false);
        return new TransactionItemHolder(inflatedView);
    }
    @Override
    public void onBindViewHolder(@NotNull myAdapter.TransactionItemHolder holder, int position) {
        Transaction transaction = Items.get(position);
        holder.bindTodoItem(transaction);
    }


    @Override
    public int getItemCount() {
        return Items.size();
    }


    public  class TransactionItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{

        TextView textViewDateReview;
        TextView textViewTypeReview;
        TextView textViewAmountReview;
        TextView textViewNoteReview;
        Transaction item;

        public TransactionItemHolder(View v) {
            super(v);
            textViewDateReview=(TextView)v.findViewById(R.id.textViewDateReview);
            textViewTypeReview=(TextView)v.findViewById(R.id.textViewTypeReview);
            textViewAmountReview=(TextView)v.findViewById(R.id.textViewAmountReview);
            textViewNoteReview=(TextView)v.findViewById(R.id.textViewNoteReview);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        public void bindTodoItem(@NotNull Transaction item){
            this.item=item;

            textViewDateReview.setText(item.getDate());
            textViewTypeReview.setText(item.getType());
            textViewAmountReview.setText(String.valueOf(item.getAmount())+" €");
            textViewNoteReview.setText(item.getNote());
        }


        @Override
        public void onClick(View v) {
            Log.v("onTouch","Click");
            Toast toast = Toast.makeText(context, "Swipe to delete,\nLong press  to modify", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP,0,0);
            toast.show();
        }

        //onLongclick start activity modify
        @Override
        public boolean onLongClick(View v) {
            int pos=getAdapterPosition();
            Transaction item;
            item=Items.get(pos);
            String s= String.valueOf(item.getAmount());
            Log.v("onTouch", s);
            Log.v("onTouch","Long Click");
            Intent intent=new Intent(context,Modify.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); ;
            intent.putExtra("item",item);
            context.startActivity(intent);
            return false;
        }
    }

}
