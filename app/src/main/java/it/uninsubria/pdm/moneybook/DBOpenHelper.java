//Riva Alessio 726395
package it.uninsubria.pdm.moneybook;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBOpenHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = "DBOpenHelper";

    // create SQL-statement
    private static final String SQL_CREATE_TABLE_MoneyBook = "create table " //
            + DBContract.TransactionItems.TABLE_NAME + " (" //
            + DBContract.TransactionItems._ID + " integer primary key autoincrement" //
            + ", " + DBContract.TransactionItems.COLUMN_NAME_TYPE+ " text not null" //
            + ", " + DBContract.TransactionItems.COLUMN_NAME_AMOUNT+ " real not null" //
            + ", " + DBContract.TransactionItems.COLUMN_NAME_NOTE+ " text" //
            + ", " + DBContract.TransactionItems.COLUMN_NAME_CREATION_DATE + " text not null" //
            + ");";

    private static DBOpenHelper sInstance;  //singleton instance

    /**
     * Constructor should be private to prevent direct instantiation.
     * make call to static method "getInstance()" instead.
     */
    private DBOpenHelper(Context context) {
        super(context, DBContract.DATABASE_NAME, null, DBContract.DATABASE_VERSION);
    }

    public static synchronized DBOpenHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null)
            sInstance = new DBOpenHelper(context.getApplicationContext());

        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) throws SQLException {
        Log.w(LOG_TAG, "Creating database.");
        try {
            db.execSQL(SQL_CREATE_TABLE_MoneyBook);
        } catch (SQLException e) {
            Log.e(LOG_TAG, e.getMessage());
            throw e;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // NOT IMPLEMENTED
    }

}
