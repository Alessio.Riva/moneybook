//Riva Alessio 726395
package it.uninsubria.pdm.moneybook;

import android.content.ContentValues;

import java.io.Serializable;
import java.sql.Date;

public class Transaction implements Serializable {

        private int ID;
        private String type,date,note;
        private float amount;
    public Transaction(String type, float amount, String date, String note){
        this.type=type;
        this.amount=amount;
        this.date=date;
        this.note=note;
    }
    public Transaction(){
    }

    public int getID() {
        return ID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }



    public void setID(int ID) {
        this.ID = ID;
    }

    //used to insert item in db
    public ContentValues getContentValue(){
            ContentValues values=new ContentValues();
            values.put(DBContract.TransactionItems.COLUMN_NAME_TYPE,this.type);
            values.put(DBContract.TransactionItems.COLUMN_NAME_AMOUNT,this.amount);
            values.put(DBContract.TransactionItems.COLUMN_NAME_NOTE,this.note);
            values.put(DBContract.TransactionItems.COLUMN_NAME_CREATION_DATE,this.date);
            return values;
    }
}
