//Riva Alessio 726395

package it.uninsubria.pdm.moneybook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CustomArrayAdapter extends ArrayAdapter<Transaction> {
    Context context;
    int resource;
    ArrayList<Transaction> items;
    public CustomArrayAdapter(@NonNull Context context, int resource, ArrayList<Transaction> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.items=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Transaction item=items.get(position);
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(resource,parent,false);
               // convertView= LayoutInflater.from(context).inflate(R.layout.listviewitem,parent,false);
        }

        TextView textViewDateReview=(TextView)convertView.findViewById(R.id.textViewDateReview);
        TextView textViewTypeReview=(TextView)convertView.findViewById(R.id.textViewTypeReview);
        TextView textViewAmountReview=(TextView)convertView.findViewById(R.id.textViewAmountReview);
        TextView textViewNoteReview=(TextView)convertView.findViewById(R.id.textViewNoteReview);

        textViewDateReview.setText(item.getDate());
        textViewTypeReview.setText(item.getType());
        textViewAmountReview.setText(String.valueOf(item.getAmount())+" €");
        textViewNoteReview.setText(item.getNote());

        return convertView;

        //return super.getView(position, convertView, parent);
    }
}
