//Riva Alessio 726395
package it.uninsubria.pdm.moneybook;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Modify extends AppCompatActivity {
    TextView textViewType,textViewAmount,textViewNote,textViewDate;
    Transaction item;
    private DBAdapter dbAdapter;
    Spinner spinner;
    String Stype[]={"salary","extra","tax","food","travel","other"};
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify);
        //textViewType= findViewById(R.id.editTextTypeM);
        textViewAmount= findViewById(R.id.editTextAmountM);
        textViewNote= findViewById(R.id.editTextNoteM);
        textViewDate=findViewById(R.id.textViewDateModify);


        dbAdapter = DBAdapter.getInstance(this);
        //dbAdapter.open();

        Intent intent=getIntent();
        item= (Transaction) intent.getSerializableExtra("item");
        //textViewType.setText(item.getType());
        textViewAmount.setText(String.valueOf(item.getAmount()));
        textViewNote.setText(item.getNote());
        textViewDate.setText(item.getDate());

        spinner=(Spinner)findViewById(R.id.spinnerModify);
        ArrayAdapter aaSpinner= new ArrayAdapter(this,android.R.layout.simple_spinner_item, Stype);
        aaSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aaSpinner);
       // String test=item.getType();
       // int value=aaSpinner.getPosition(test);
       // spinner.setSelection(value);

        spinner.setSelection(aaSpinner.getPosition(item.getType()));


    }

    @Override
    protected void onPause() {
        super.onPause();
        //dbAdapter.close();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //dbAdapter.close();
    }

    public void saveOnClick(View v){
        String type,note,date;
        float amount;
        if(TextUtils.isEmpty(textViewAmount.getText()) || TextUtils.isEmpty(textViewNote.getText())){
            Toast toast = Toast.makeText(getApplicationContext(), "no empty fields allowed", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }else {
            //type= String.valueOf(textViewType.getText()).toLowerCase();
            type = String.valueOf(spinner.getSelectedItem());
            amount = Float.parseFloat(textViewAmount.getText().toString());
            date = String.valueOf(textViewDate.getText());
            note = String.valueOf(textViewNote.getText());

            Transaction update = new Transaction(type, amount, date, note);

            dbAdapter.updateItem(update, item.getID());
            finish();
        }

    }
}
/* yearSpinner=(Spinner)findViewById(R.id.spinnerYear);
       // monthSpinner=findViewById(R.id.spinnerMonth);
       // daySpinner=findViewById(R.id.spinnerDay);

       // yearList=new ArrayList<String>();
       // monthList=new ArrayList<String>();
       // yearList=new ArrayList<String>();

        String[] year={"2020","2019","2018","2017"};
        int i;
        String[] month = new String[12];
        //for(i=0,i<=11,i++){month[i]=String.valueOf(i+1);}
        //String[] day =new String[31];
        //for(i=0,i<=31,i++){day[i]=String.valueOf(i+1);}

        yearList.addAll(Arrays.asList(year));
        //ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, yearList);;
        ArrayAdapter adapter1=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,yearList);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(adapter1);*/