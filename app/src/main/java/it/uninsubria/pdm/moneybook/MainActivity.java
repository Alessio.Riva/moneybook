//Riva Alessio 726395
package it.uninsubria.pdm.moneybook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import it.uninsubria.pdm.moneybook.DBAdapter;
import it.uninsubria.pdm.moneybook.R;

public class MainActivity extends AppCompatActivity {
    TextView textViewDate, textViewType, textViewAmount, textViewNote;
    TextView textViewSalaryTMonth,textViewSalaryTYear,textViewExtraTMonth,textViewExtraTYear,textViewTaxTMonth,textViewTaxTYear,textViewFoodTMonth,textViewFoodTYear,textViewTravelTMonth,textViewTravelTYear,textViewOtherTM,textViewOtherTY,textViewTotalTM,textViewTotalTY;
    private DBAdapter dbAdapter;
    Spinner spinner;
    String Stype[]={"Salary","Extra","Tax","Food","Travel","Other"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        textViewDate = (TextView) findViewById(R.id.textViewDate);
        //textViewType = (TextView) findViewById(R.id.textViewType);
        textViewAmount = (TextView) findViewById(R.id.textViewAmount);
        textViewNote = (TextView) findViewById(R.id.textViewNote);
        textViewSalaryTMonth=(TextView) findViewById(R.id.textViewSalaryTMonth);
        textViewSalaryTYear=(TextView) findViewById(R.id.textViewSalaryTYear);
        textViewExtraTMonth=(TextView) findViewById(R.id.textViewExtraTMonth);
        textViewExtraTYear=(TextView) findViewById(R.id.textViewExtraTYear);
        textViewTaxTMonth=(TextView) findViewById(R.id.textViewTaxTMonth);
        textViewTaxTYear=(TextView) findViewById(R.id.textViewTaxTYear);
        textViewFoodTMonth=(TextView) findViewById(R.id.textViewFoodTMonth);
        textViewFoodTYear=(TextView) findViewById(R.id.textViewFoodTYear);
        textViewTravelTMonth=(TextView) findViewById(R.id.textViewTravelTmonth);
        textViewTravelTYear=(TextView) findViewById(R.id.textViewTravelTYear);
        textViewOtherTM=(TextView) findViewById(R.id.textViewOtherTMonth);
        textViewOtherTY=(TextView) findViewById(R.id.textViewOtherTYear);
        textViewTotalTM=(TextView) findViewById(R.id.textViewTotalTM);
        textViewTotalTY=(TextView) findViewById(R.id.textViewTotalTY);
        setDate(textViewDate);

        spinner=(Spinner)findViewById(R.id.spinnerMain);
        ArrayAdapter aaSpinner= new ArrayAdapter(this,android.R.layout.simple_spinner_item, Stype);
        aaSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aaSpinner);

        //dbAdapter.prova();
        updateData();
        //testinsert();

    }

    @Override
    protected void onResume() {
        super.onResume();
        dbAdapter.open();
        updateData();


    }

    @Override
    protected void onPause() {
        super.onPause();
        dbAdapter.close();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapter.close();
    }

    public void setDate(TextView view) {

        Date today = Calendar.getInstance().getTime();//getting date
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");//formating according to my need
        String date = formatter.format(today);
        view.setText(date);
    }

    public void onClickAdd(View v) {

        //textViewNote.setText(textViewDate.getText().toString() + textViewAmount.getText().toString() + textViewType.getText().toString());
        //dbAdapter.insert(transaction);
        //textViewNote.setText((int) id);
        if(TextUtils.isEmpty(textViewAmount.getText()) || TextUtils.isEmpty(textViewNote.getText())){
            Toast toast = Toast.makeText(getApplicationContext(), "no empty fields allowed", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }else{
            String s= String.valueOf(spinner.getSelectedItem());
            Transaction transaction=new Transaction(s.toLowerCase(),Float.parseFloat(textViewAmount.getText().toString()),textViewDate.getText().toString(),textViewNote.getText().toString());
            //Transaction transaction= new Transaction("tax","20","10092019","prova");
            long id=dbAdapter.insert(transaction);
            Log.v("Insert:", String.valueOf(id));
        }

        //dbAdapter.close();
        closeKeyboard();
        textViewAmount.clearFocus();
        //textViewType.clearFocus();
        textViewNote.clearFocus();

        updateData();

    }
    public void onClickReview(View v){
        Intent i= new Intent(this,Review.class);
        startActivity(i);
    }
    public void updateData(){
       float salaryM,salaryY,extraM,extraY,taxM,taxY,foodM,foodY,travelM,travelY,otherM,otherY,totalM,totalY;
        salaryM=dbAdapter.getSalaryMonth();
        salaryY=dbAdapter.getSalaryYear();
        extraM=dbAdapter.getExtraMonth();
        extraY=dbAdapter.getExtraYear();
        taxM=dbAdapter.getTaxMonth();
        taxY=dbAdapter.getTaxYear();
        foodM=dbAdapter.getFoodMonth();
        foodY=dbAdapter.getFoodYear();
        travelM=dbAdapter.getTravelMonth();
        travelY=dbAdapter.getTravelYear();
        otherM=dbAdapter.getOtherMonth();
        otherY=dbAdapter.getOtherYear();
        totalM=dbAdapter.getTotalMonth();
        totalY=dbAdapter.getTotalYear();

        textViewSalaryTMonth.setText(String.valueOf(salaryM));
        textViewSalaryTYear.setText(String.valueOf(salaryY));
        textViewExtraTMonth.setText(String.valueOf(extraM));
        textViewExtraTYear.setText(String.valueOf(extraY));
        textViewTaxTMonth.setText(String.valueOf(taxM));
        textViewTaxTYear.setText(String.valueOf(taxY));
        textViewFoodTMonth.setText(String.valueOf(foodM));
        textViewFoodTYear.setText(String.valueOf(foodY));
        textViewTravelTMonth.setText(String.valueOf(travelM));
        textViewTravelTYear.setText(String.valueOf(travelY));
        textViewOtherTM.setText(String.valueOf(otherM));
        textViewOtherTY.setText(String.valueOf(otherY));
        textViewTotalTM.setText(String.valueOf(totalM)+"€");
        textViewTotalTY.setText(String.valueOf(totalY)+"€");


    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //test item
    void testinsert(){
        Transaction t1=new Transaction("tax",77,"2019-01-20","prova tax");
        Transaction t2=new Transaction("food",55,"2018-12-10","prova food");
        Transaction t3=new Transaction("salary",1200,"2020-01-25","prova salary");
        Transaction t4=new Transaction("extra",125,"2020-03-15","prova extra");
        dbAdapter.insert(t1);
        dbAdapter.insert(t2);
        dbAdapter.insert(t3);
        dbAdapter.insert(t4);

    }
}