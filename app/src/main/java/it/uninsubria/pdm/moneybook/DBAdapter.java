//Riva Alessio 726395
package it.uninsubria.pdm.moneybook;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

class DBAdapter {

    // the tag used in LogCat messages
    private static String TAG = "DBAdapter";

    private static DBAdapter sInstance;  //singleton instance

    // ADAPTER STATE
    private SQLiteDatabase db; // reference to the DB
    private DBOpenHelper dbHelper; // reference to the OpenHelper

    static synchronized DBAdapter getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null)
            sInstance = new DBAdapter(context.getApplicationContext());

        return sInstance;
    }

    private DBAdapter(Context context) {
        this.dbHelper = DBOpenHelper.getInstance(context);
    }

    /**
     * Open the DB in write mode. If the DB cannot be opened in write mode, the
     * method throws an exception to signal the failure
     *
     * @return this (self reference, allowing this to be chained in an
     * initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    DBAdapter open() throws SQLException {
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());
            throw e;
        }
        return this;
    }

    /**
     * Close the DB.
     */
    void close() {
        db.close();
    }

    /**
     * Add the specified element to the DB.
     *
     * @param item
     * @return @return rowId or -1 if failed.
     */



    //method used in activity main

    long insert(Transaction item) {
        //ContentValues cv = item.getContentValue();
        long idx = db.insert(DBContract.TransactionItems.TABLE_NAME, null, item.getContentValue());
        // we set here the idx
        //item.setID(idx);
        Log.w(TAG, "Added value idx: " + idx);
        return idx;
    }
    float getSalaryMonth(){

        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='salary' and strftime('%Y',Date)='"+getYear()+"' and date like '%%%%-"+getMonth()+"-%%'", null);
        myCursor.moveToFirst();
        float salary= myCursor.getFloat(0);

        return salary;

    }
    float getSalaryYear(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='salary' and strftime('%Y',Date)= '"+getYear()+"'", null);
        myCursor.moveToFirst();
        float salary= myCursor.getFloat(0);
        return salary;
    }
    float getExtraMonth(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='extra' and strftime('%Y',Date)='"+getYear()+"' and date like '%%%%-"+getMonth()+"-%%'", null);
        myCursor.moveToFirst();
        float extra= myCursor.getFloat(0);
        return extra;
    }
    float getExtraYear(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='extra' and strftime('%Y',Date)= '"+getYear()+"'", null);
        myCursor.moveToFirst();
        float extra= myCursor.getFloat(0);
        return extra;
    }
    float getTaxMonth(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='tax' and strftime('%Y',Date)='"+getYear()+"' and date like '%%%%-"+getMonth()+"-%%'", null);
        myCursor.moveToFirst();
        float tax= myCursor.getFloat(0);
        return tax;
    }
    float getTaxYear(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='tax' and strftime('%Y',Date)= '"+getYear()+"'", null);
        myCursor.moveToFirst();
        float tax= myCursor.getFloat(0);
        return tax;
    }
    float getFoodMonth(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='food' and strftime('%Y',Date)='"+getYear()+"' and date like '%%%%-"+getMonth()+"-%%'", null);
        myCursor.moveToFirst();
        float food= myCursor.getFloat(0);
        return food;
    }
    float getFoodYear(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='food' and strftime('%Y',Date)= '"+getYear()+"'", null);
        myCursor.moveToFirst();
        float food= myCursor.getFloat(0);
        return food;
    }
    float getTravelMonth(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='travel' and strftime('%Y',Date)='"+getYear()+"' and date like '%%%%-"+getMonth()+"-%%'", null);
        myCursor.moveToFirst();
        float travel= myCursor.getFloat(0);
        return travel;
    }
    float getTravelYear(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='travel' and strftime('%Y',Date)= '"+getYear()+"'", null);
        myCursor.moveToFirst();
        float travel= myCursor.getFloat(0);
        return travel;
    }
    float getOtherMonth(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='other' and strftime('%Y',Date)='"+getYear()+"' and date like '%%%%-"+getMonth()+"-%%'", null);
        myCursor.moveToFirst();
        float other= myCursor.getFloat(0);
        return other;
    }
    float getOtherYear(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='other' and strftime('%Y',Date)= '"+getYear()+"'", null);
        myCursor.moveToFirst();
        float other= myCursor.getFloat(0);
        return other;
    }
    float getTotalMonth(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where strftime('%Y',Date)='"+getYear()+"' and date like '%%%%-"+getMonth()+"-%%' and (type='salary' or type='extra')", null);
        myCursor.moveToFirst();
        float total= myCursor.getFloat(0);
        myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where strftime('%Y',Date)='"+getYear()+"' and date like '%%%%-"+getMonth()+"-%%' and (type='tax' or type='food' or type='other' or type='travel')", null);
        myCursor.moveToFirst();
        float minus= myCursor.getFloat(0);
        total=total-minus;
        return total;
    }
    float getTotalYear(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where strftime('%Y',Date)= '"+getYear()+"' and (type='salary' or type='extra')", null);
        myCursor.moveToFirst();
        float total= myCursor.getFloat(0);
        myCursor=db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where strftime('%Y',Date)= '"+getYear()+"' and (type='tax' or type='food' or type='other' or type='travel')", null);
        myCursor.moveToFirst();
        float minus=myCursor.getFloat(0);
        total=total-minus;
        return total;
    }

    //method used in review activity
    float getIncome(){
       Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='salary' or type='extra' ", null);
       myCursor.moveToFirst();
       float f= myCursor.getFloat(0);
       return f;
    }
    float getOutcome(){
        Cursor myCursor= db.rawQuery("select sum(amount) from "+DBContract.TransactionItems.TABLE_NAME+" where type='tax' or type='food' or type='travel' or type='other'", null);
        myCursor.moveToFirst();
        float f= myCursor.getFloat(0);
        return f;
    }
    boolean deleteTransaction(long idx) {
        return db.delete(DBContract.TransactionItems.TABLE_NAME, DBContract.TransactionItems._ID + "=" + idx, null) == 1;
    }
    //iD,type,amount,note,creationDate
    synchronized ArrayList<Transaction> getAllItems(){
        ArrayList<Transaction> Items=new ArrayList<Transaction>();
        String query = "SELECT * FROM " + DBContract.TransactionItems.TABLE_NAME+" ORDER BY date DESC";

        Cursor myCursor=db.rawQuery(query,null);
        Transaction item=null;
        if(myCursor.moveToFirst()){
            do{
                item=new Transaction();
                item.setID(Integer.parseInt(myCursor.getString(0)));
                item.setType(myCursor.getString(1));
                item.setAmount(myCursor.getFloat(2));
                item.setNote(myCursor.getString(3));
                item.setDate(myCursor.getString(4));
                Items.add(item);
            }while (myCursor.moveToNext());


        }
        return Items;
    }
    ArrayList<Transaction> getAllSalary(){
        ArrayList<Transaction> Items=new ArrayList<Transaction>();
        String query = "SELECT * FROM " + DBContract.TransactionItems.TABLE_NAME+" where type ='salary' ORDER BY date DESC";

        Cursor myCursor=db.rawQuery(query,null);
        Transaction item=null;
        if(myCursor.moveToFirst()){
            do{
                item=new Transaction();
                item.setID(Integer.parseInt(myCursor.getString(0)));
                item.setType(myCursor.getString(1));
                item.setAmount(myCursor.getFloat(2));
                item.setNote(myCursor.getString(3));
                item.setDate(myCursor.getString(4));
                Items.add(item);
            }while (myCursor.moveToNext());


        }
        return Items;
    }
    ArrayList<Transaction> getAllExtra(){
        ArrayList<Transaction> Items=new ArrayList<Transaction>();
        String query = "SELECT * FROM " + DBContract.TransactionItems.TABLE_NAME+" where type='extra' ORDER BY date DESC";

        Cursor myCursor=db.rawQuery(query,null);
        Transaction item=null;
        if(myCursor.moveToFirst()){
            do{
                item=new Transaction();
                item.setID(Integer.parseInt(myCursor.getString(0)));
                item.setType(myCursor.getString(1));
                item.setAmount(myCursor.getFloat(2));
                item.setNote(myCursor.getString(3));
                item.setDate(myCursor.getString(4));
                Items.add(item);
            }while (myCursor.moveToNext());


        }
        return Items;
    }
    ArrayList<Transaction> getAllTax(){
        ArrayList<Transaction> Items=new ArrayList<Transaction>();
        String query = "SELECT * FROM " + DBContract.TransactionItems.TABLE_NAME+" where type='tax' ORDER BY date DESC";

        Cursor myCursor=db.rawQuery(query,null);
        Transaction item=null;
        if(myCursor.moveToFirst()){
            do{
                item=new Transaction();
                item.setID(Integer.parseInt(myCursor.getString(0)));
                item.setType(myCursor.getString(1));
                item.setAmount(myCursor.getFloat(2));
                item.setNote(myCursor.getString(3));
                item.setDate(myCursor.getString(4));
                Items.add(item);
            }while (myCursor.moveToNext());


        }
        return Items;
    }
    ArrayList<Transaction> getAllFood(){
        ArrayList<Transaction> Items=new ArrayList<Transaction>();
        String query = "SELECT * FROM " + DBContract.TransactionItems.TABLE_NAME+" where type='food' ORDER BY date DESC";

        Cursor myCursor=db.rawQuery(query,null);
        Transaction item=null;
        if(myCursor.moveToFirst()){
            do{
                item=new Transaction();
                item.setID(Integer.parseInt(myCursor.getString(0)));
                item.setType(myCursor.getString(1));
                item.setAmount(myCursor.getFloat(2));
                item.setNote(myCursor.getString(3));
                item.setDate(myCursor.getString(4));
                Items.add(item);
            }while (myCursor.moveToNext());


        }
        return Items;
    }
    ArrayList<Transaction> getAllTravel(){
        ArrayList<Transaction> Items=new ArrayList<Transaction>();
        String query = "SELECT * FROM " + DBContract.TransactionItems.TABLE_NAME+" where type='travel' ORDER BY date DESC";

        Cursor myCursor=db.rawQuery(query,null);
        Transaction item=null;
        if(myCursor.moveToFirst()){
            do{
                item=new Transaction();
                item.setID(Integer.parseInt(myCursor.getString(0)));
                item.setType(myCursor.getString(1));
                item.setAmount(myCursor.getFloat(2));
                item.setNote(myCursor.getString(3));
                item.setDate(myCursor.getString(4));
                Items.add(item);
            }while (myCursor.moveToNext());


        }
        return Items;
    }
    ArrayList<Transaction> getAllOther(){
        ArrayList<Transaction> Items=new ArrayList<Transaction>();
        String query = "SELECT * FROM " + DBContract.TransactionItems.TABLE_NAME+" where type='other' ORDER BY date DESC";

        Cursor myCursor=db.rawQuery(query,null);
        Transaction item=null;
        if(myCursor.moveToFirst()){
            do{
                item=new Transaction();
                item.setID(Integer.parseInt(myCursor.getString(0)));
                item.setType(myCursor.getString(1));
                item.setAmount(myCursor.getFloat(2));
                item.setNote(myCursor.getString(3));
                item.setDate(myCursor.getString(4));
                Items.add(item);
            }while (myCursor.moveToNext());


        }
        return Items;
    }

    String getMonth(){
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH)+1;
        String s="";
        if(month<=10)
            s="0";
        return s+String.valueOf(month);
    }
    String getYear(){
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        String s=String.valueOf(year);
        return s;
    }

    //modify activity
    void updateItem(Transaction item, int id){
        db.update(DBContract.TransactionItems.TABLE_NAME,item.getContentValue(),"id="+id,null);
        /*String s=item.getType();
        Log.v("type",s);
        String a= String.valueOf(item.getAmount());
        Log.v("amount",a);
        String b=item.getDate();
        Log.v("date",b);
        String c=item.getNote();
        Log.v("note",c);*/
    }


    void deleteAll(){
        db.execSQL("DELETE FROM "+DBContract.TransactionItems.TABLE_NAME);
    }

}